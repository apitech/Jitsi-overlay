toMetrics = (object) => {
    let metrics = '';
    for (propName in object){
        metrics += `${propName} ${object[propName]}\n`;
    }

    return metrics;
}

module.exports.metrics = async(req, res, next) => {

    if (process.env.METRICS_IPS){

        var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null;

        if (process.env.METRICS_IPS === '*' || process.env.METRICS_IPS.split(',').includes(ip)){
            res.setHeader('content-type', 'text/plain');

            const metricsCache = req.app.locals.metricsCache;

            let metrics = {};
            metrics['lastTenAverageRates'] = 0;
            metrics['lastTenCountRates'] = 0;

            if (metricsCache && metricsCache.hasOwnProperty('rates')){
                const sum = metricsCache.rates.reduce((a, b) => a + b.evaluation, 0);
                const avg = (sum / metricsCache.rates.length) || 0;
                metrics['lastTenAverageRates'] = avg;
                metrics['lastTenCountRates'] = metricsCache.rates.length;
            }

            res.send(toMetrics(metrics));
        }
    }
};