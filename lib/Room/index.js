module.exports = (req, res, next) => {

    var room = req.params['room'];
    //var validator = require('validator');

    //if (room !== '' && validator.isAlphanumeric(room)) {
    res.render('Room/index', { from_electron: req.query.from_electron, room: room, jwt: req.query.jwt });
    //res.sendFile('/usr/share/jitsi-meet/index.html');

    // } else {
    //     res.redirect('/?e=' + encodeURIComponent('Nom de salon incorrect.\nLe nom ne doit pas comporter de caractères spéciaux.'));
    // }
};

module.exports.rate = async(req, res, next) => {

    var mongoHelper = require('../Shared/MongoHelper');

    var rate = req.body;
    rate.date = new Date();

    mongoHelper.mongoInsert(process.env.MONGO_DB, process.env.MONGO_COLLECTION, rate);

    const metricsCache = req.app.locals.metricsCache;

    if (metricsCache && !metricsCache.hasOwnProperty('rates')){
        metricsCache.rates = [];
    }
    else {
        // On conserve uniquement les 10 derniers votes
        metricsCache.rates.splice(0, metricsCache.rates.length - 9);
    }

    metricsCache.rates.push(rate);
};