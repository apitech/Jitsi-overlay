exports.mongoConnectAsync = function(dbName) {
    return new Promise(resolve => {
        const uri = "mongodb://" + process.env.MONGO_SERVER + "/" + dbName + "?retryWrites=true&w=majority";
        const MongoClient = require('mongodb').MongoClient;
        const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
        client.connect(async error => {
            resolve(client, error);
        });
    });
}

exports.mongoFindAsync = function(dbName, collectionName, findExpression) {
    return new Promise(resolve => {
        var mongoHelper = require('../Shared/MongoHelper');

        mongoHelper.mongoConnectAsync(dbName)
            .then(async(client, error) => {
                const collection = client.db(dbName).collection(collectionName);
                var results = await collection.find(findExpression).toArray();

                client.close();

                resolve(results);
            });
    });
}

exports.mongoDeleteAsync = function(dbName, collectionName, deleteExpression) {
    return new Promise(resolve => {
        var mongoHelper = require('../Shared/MongoHelper');

        mongoHelper.mongoConnectAsync(dbName)
            .then(async(client, error) => {
                const collection = client.db(dbName).collection(collectionName);

                collection.deleteMany(deleteExpression, function(error) {
                    if (error) console.log(error);
                    client.close();
                });

                resolve();
            });
    });
}

exports.mongoUpsertAsync = function(dbName, collectionName, findExpression, updateExpression) {
    return new Promise(resolve => {
        var mongoHelper = require('../Shared/MongoHelper');

        mongoHelper.mongoConnectAsync(dbName)
            .then(async(client, error) => {
                const collection = client.db(dbName).collection(collectionName);

                let doc = await collection.findOneAndUpdate(findExpression, updateExpression, {
                    new: true,
                    upsert: true // Make this update into an upsert
                });

                resolve();
            });
    });
}

exports.mongoInsert = function(dbName, collectionName, document) {
    return new Promise(resolve => {
        var mongoHelper = require('../Shared/MongoHelper');

        mongoHelper.mongoConnectAsync(dbName)
            .then(async(client, error) => {
                const collection = client.db(dbName).collection(collectionName);

                let doc = await collection.insert(document);

                resolve();
            });
    });
}