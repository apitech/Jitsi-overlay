exports.diacriticSensitiveRegex = function(string = '') {
    return string.replace(/a/g, '[a,á,à,ä]')
        .replace(/e/g, '[e,é,ë]')
        .replace(/i/g, '[i,í,ï]')
        .replace(/o/g, '[o,ó,ö,ò]')
        .replace(/u/g, '[u,ü,ú,ù]');
}

/// --------------------------------------------------------
/// Custom string encoding, even more strict than RFC3986
/// - Everything after '#' or '?' is ignored
/// - Characters '-', '_' and '.' remain unencoded
/// - Every other character is encoded
/// --------------------------------------------------------
exports.customEncodeURIComponent = function(str)
{
    let returnString = str;

    // Step 1 : remove everything after # or ?
    if(returnString.indexOf('#') !== -1) // returnString.includes('#')
    {
        returnString = returnString.substr(0, returnString.indexOf('#'));
    }
    if(returnString.indexOf('?') !== -1) // returnString.includes('?')
    {
        returnString = returnString.substr(0, returnString.indexOf('?'));
    }

    // Step 2 : use encodeComponent
    returnString = encodeURIComponent(returnString);

    // Step 3 : manually encode characters to reach RFC3986
    returnString = returnString.replace(/[!]/g, '%21');
    returnString = returnString.replace(/[']/g, '%27');
    returnString = returnString.replace(/[(]/g, '%28');
    returnString = returnString.replace(/[)]/g, '%29');
    returnString = returnString.replace(/[*]/g, '%2A');

    // Step 4 : manually encode other characters to reach full encoding
    returnString = returnString.replace(/[~]/g, '%7E');
    //returnString = returnString.replace(/[.]/g, '%2E');
    //returnString = returnString.replace(/[-]/g, '%2D');
    //returnString = returnString.replace(/[_]/g, '%5F');

    // Step 5 : return
    return returnString;
}

/*
// @source https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent#Description
// @see also https://stackoverflow.com/a/18251730/8179249 and RFC 3986
exports.rfc3986EncodeURIComponent = function(str)
{
    function unsupportedCharReplacer(c)
    {
        return encodeURI('%' + c.charCodeAt(0).toString(16));
    }

    return encodeURIComponent(str).replace(/[!'()*]/g, unsupportedCharReplacer);
}
*/
