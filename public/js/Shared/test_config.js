/* js/pages/test_browser_page.js */
var tests = [testBrowser, testGetDefaultAudioCapture, testGetDefaultVideoCapture, testListDevices /*, testMediaConnexion*/ ];
var testRunning = false;
var runTestTimeout;

//iceServer = [{ "urls": "", "username": "1588774595", "credential": "jZBjclmuylhQHbUmW4VE8wB3nRU=", "credentialType": "password", "udpTestUrl": "turn:ix1-dv-u16-GN42JRA4T5-Turn-02.renater.fr?transport=udp", "tcpTestUrl": "turn:ix1-dv-u16-GN42JRA4T5-Turn-02.renater.fr:443?transport=tcp", "uris": ["stun:[2001:738:0:51a::44]:80?transport=tcp", "stun:[2001:738:0:51a::44]:80?transport=udp", "stun:[2001:738:0:51a::44]:3478?transport=udp", "stun:[2001:738:0:51a::44]:3478?transport=tcp", "turn:[2001:738:0:51a::44]:80?transport=tcp", "turns:[2001:738:0:51a::44]:443?transport=tcp", "turn:[2001:738:0:51a::44]:80?transport=udp", "turns:[2001:738:0:51a::44]:443?transport=udp", "turn:[2001:738:0:51a::44]:3478?transport=udp", "turn:[2001:738:0:51a::44]:3478?transport=tcp", "turns:[2001:738:0:51a::44]:5349?transport=udp", "turns:[2001:738:0:51a::44]:5349?transport=tcp", "stun:193.225.25.44:80?transport=tcp", "stun:193.225.25.44:80?transport=udp", "stun:193.225.25.44:3478?transport=udp", "stun:193.225.25.44:3478?transport=tcp", "turn:193.225.25.44:80?transport=tcp", "turns:193.225.25.44:443?transport=tcp", "turn:193.225.25.44:80?transport=udp", "turns:193.225.25.44:443?transport=udp", "turn:193.225.25.44:3478?transport=udp", "turn:193.225.25.44:3478?transport=tcp", "turns:193.225.25.44:5349?transport=udp", "turns:193.225.25.44:5349?transport=tcp", "stun:193.136.47.72:80?transport=tcp", "stun:193.136.47.72:80?transport=udp", "stun:193.136.47.72:3478?transport=udp", "stun:193.136.47.72:3478?transport=tcp", "turn:193.136.47.72:80?transport=tcp", "turns:193.136.47.72:443?transport=tcp", "turn:193.136.47.72:80?transport=udp", "turns:193.136.47.72:443?transport=udp", "turn:193.136.47.72:3478?transport=udp", "turn:193.136.47.72:3478?transport=tcp", "turns:193.136.47.72:5349?transport=udp", "turns:193.136.47.72:5349?transport=tcp", "stun:[2001:690:a00:4100::72]:80?transport=tcp", "stun:[2001:690:a00:4100::72]:80?transport=udp", "stun:[2001:690:a00:4100::72]:3478?transport=udp", "stun:[2001:690:a00:4100::72]:3478?transport=tcp", "turn:[2001:690:a00:4100::72]:80?transport=tcp", "turns:[2001:690:a00:4100::72]:443?transport=tcp", "turn:[2001:690:a00:4100::72]:80?transport=udp", "turns:[2001:690:a00:4100::72]:443?transport=udp", "turn:[2001:690:a00:4100::72]:3478?transport=udp", "turn:[2001:690:a00:4100::72]:3478?transport=tcp", "turns:[2001:690:a00:4100::72]:5349?transport=udp", "turns:[2001:690:a00:4100::72]:5349?transport=tcp"] }];
iceServer = [{ "urls": "", "username": "1588774595", "credential": "jZBjclmuylhQHbUmW4VE8wB3nRU=", "credentialType": "password", "udpTestUrl": "turn:joona.icu?transport=udp", "tcpTestUrl": "turn:joona.icu:443?transport=tcp", "uris": ["stun:meet-jit-si-turnrelay.jitsi.net:443?transport=tcp", "stun:joona.icu:80?transport=udp", "stun:joona.icu:3478?transport=udp", "stun:joona.icu:3478?transport=tcp", "turn:[2001:738:0:51a::44]:80?transport=tcp", "turns:[2001:738:0:51a::44]:443?transport=tcp", "turn:[2001:738:0:51a::44]:80?transport=udp", "turns:[2001:738:0:51a::44]:443?transport=udp", "turn:[2001:738:0:51a::44]:3478?transport=udp", "turn:[2001:738:0:51a::44]:3478?transport=tcp", "turns:[2001:738:0:51a::44]:5349?transport=udp", "turns:[2001:738:0:51a::44]:5349?transport=tcp", "stun:193.225.25.44:80?transport=tcp", "stun:193.225.25.44:80?transport=udp", "stun:193.225.25.44:3478?transport=udp", "stun:193.225.25.44:3478?transport=tcp", "turn:193.225.25.44:80?transport=tcp", "turns:193.225.25.44:443?transport=tcp", "turn:193.225.25.44:80?transport=udp", "turns:193.225.25.44:443?transport=udp", "turn:193.225.25.44:3478?transport=udp", "turn:193.225.25.44:3478?transport=tcp", "turns:193.225.25.44:5349?transport=udp", "turns:193.225.25.44:5349?transport=tcp", "stun:193.136.47.72:80?transport=tcp", "stun:193.136.47.72:80?transport=udp", "stun:193.136.47.72:3478?transport=udp", "stun:193.136.47.72:3478?transport=tcp", "turn:193.136.47.72:80?transport=tcp", "turns:193.136.47.72:443?transport=tcp", "turn:193.136.47.72:80?transport=udp", "turns:193.136.47.72:443?transport=udp", "turn:193.136.47.72:3478?transport=udp", "turn:193.136.47.72:3478?transport=tcp", "turns:193.136.47.72:5349?transport=udp", "turns:193.136.47.72:5349?transport=tcp", "stun:[2001:690:a00:4100::72]:80?transport=tcp", "stun:[2001:690:a00:4100::72]:80?transport=udp", "stun:[2001:690:a00:4100::72]:3478?transport=udp", "stun:[2001:690:a00:4100::72]:3478?transport=tcp", "turn:[2001:690:a00:4100::72]:80?transport=tcp", "turns:[2001:690:a00:4100::72]:443?transport=tcp", "turn:[2001:690:a00:4100::72]:80?transport=udp", "turns:[2001:690:a00:4100::72]:443?transport=udp", "turn:[2001:690:a00:4100::72]:3478?transport=udp", "turn:joona.icu:3478?transport=tcp", "turns:joona.icu:5349?transport=udp", "turns:joona.icu:5349?transport=tcp"] }];

function setInProgress(index) {
    console.log('setInProgress ' + index);
    var item = $('.test-item')[index];

    $(item).removeClass("success");
    $(item).removeClass("error");
    $(item).removeClass("inactive");
    $(item).addClass("current");
}

function setSuccess(index, message) {
    var item = $('.test-item')[index];

    $(item).removeClass("current");
    $(item).removeClass("error");
    $(item).removeClass("inactive");
    $(item).addClass("success");

    if (message == undefined || message == null || message == '')
        message = 'Aucun problème détecté';
    setMessage(index, message);
}

function setMessage(index, message) {
    if (message !== undefined) {

        var fullMessage = $($('.test-item p')[index]).prop('title');

        if (fullMessage == undefined || fullMessage == null)
            fullMessage = '';

        fullMessage += message;

        var idx = fullMessage.indexOf('<br/>', 0);
        if (idx >= 0) {
            idx = fullMessage.indexOf('<br/>', idx + 5);
            if (idx >= 0) {
                idx = fullMessage.indexOf('<br/>', idx + 5);
            }
        }

        var truncatedHtml = fullMessage;
        if (idx >= 0) {
            truncatedHtml = fullMessage.substring(0, idx);
            truncatedHtml += ' <b>...</b>';
        }

        $($('.test-item span')[index]).html(truncatedHtml);

        fullMessage = fullMessage.replace(new RegExp("<br/>", 'g'), "\n");

        $($('.test-item p')[index]).prop('title', fullMessage);
    }
}

function setError(index, error) {
    var item = $('.test-item')[index];

    $(item).removeClass("current");
    $(item).removeClass("success");
    $(item).removeClass("inactive");
    $(item).addClass("error");

    if (error === undefined || error == null || error == '')
        error = 'Problème détecté';

    setMessage(index, error);
}

function resetAll() {
    console.log('resetAll');
    $('.test-item').each(function(i, item) {
        $(item).removeClass("current");
        $(item).removeClass("error");
        $(item).removeClass("success");
        $(item).addClass("inactive");
    });

    $('.test-item span').each(function(i, span) {
        $(span).html("");
    });

    $('.test-item p').each(function(i, p) {
        $(p).prop('title', '');
    });
}

function startWebRTCTest() {
    console.log("startWebRTCTest Started");
    if (!testRunning) {
        testRunning = true;
        resetAll();
        if (runTestTimeout)
            clearTimeout(runTestTimeout);
        runTest(0);
    }
}

function stopWebRTCTest() {
    testRunning = false;
    resetAll();
}

function runTest(index) {
    setInProgress(index);

    console.log("runTest " + index);
    if (index < tests.length) {
        runTestTimeout = setTimeout(function() {
                console.log("Run test " + index + "/" + tests.length);
                tests[index](index);
            },
            1000);
    } else {
        testRunning = false;
        $("#reRunButton").removeClass("hide");
    }
}

function testBrowser(index) {
    var hasGUM = isGetUserMediaSupported();
    var hasPeerConnection = isPeerConnectionSupported();
    var bannedBrowser = isBannedBrowser();

    if (hasGUM && hasPeerConnection && !bannedBrowser) {
        setSuccess(index);
        runTest(++index);
    } else {
        setError();
        testRunning = false;
    }
}

function testGetDefaultAudioCapture(index) {
    getDefaultMediaCapture("audio", function(deviceName) {
            setSuccess(index);
            runTest(++index);
        },
        function(error) {

            var message = "Impossible d'utiliser votre microphone.<br/>";
            message += "Vérifiez que votre microphone est fonctionnel et bien autorisé dans votre navigateur.<br/>";
            //message += error;

            setError(index, message);
            runTest(++index);
        });
}

function testGetDefaultVideoCapture(index) {
    getDefaultMediaCapture("video", function(deviceName) {
            setSuccess(index);
            runTest(++index);
        },
        function(error) {

            var message = "Impossible d'utiliser votre Caméra.<br/>";
            message += "Vérifiez que votre caméra est fonctionnelle et bien autorisée dans votre navigateur.<br/>";
            //message += error;

            setError(index, message);
            runTest(++index);
        });
}

function testListDevices(index) {
    getListDevices(function(mediaInfo) {
            var message = '';
            mediaInfo.audioinput.forEach(function(deviceLabel) {
                if (deviceLabel != null && deviceLabel != undefined && deviceLabel != '')
                    message += deviceLabel + "<br/>";
            });
            mediaInfo.audiooutput.forEach(function(deviceLabel) {
                if (deviceLabel != null && deviceLabel != undefined && deviceLabel != '')
                    message += deviceLabel + "<br/>";
            });
            mediaInfo.videoinput.forEach(function(deviceLabel) {
                if (deviceLabel != null && deviceLabel != undefined && deviceLabel != '')
                    message += deviceLabel + "<br/>";
            });

            if (message == '') {
                setError(index, 'Aucun périphérique détecté');
            } else {
                setSuccess(index, message);
            }

            runTest(++index);
        },
        function(error) {
            setError(index, error);
            runTest(++index);
        });
}

function testMediaConnexion(index) {
    initTurnServerCerdentials();
    testMediaConnexionUDP(index);
}

function testMediaConnexionUDP(index) {
    console.log("Test udp media network");

    var message = ''
    var oneFailed = false;
    initiateMediaConnexion("udp",
        function(connected) {
            if (connected) {
                message = "UDP : OK<br>";
            } else {
                message = "UDP : KO<br>";
                oneFailed = true;
            }

            testMediaConnexionTCP(index, message, oneFailed);
        },
        function(error) {
            message = "UDP : KO<br>";
            oneFailed = true;

            testMediaConnexionTCP(index, message, oneFailed);
        }
    );
}

function testMediaConnexionTCP(index, message, oneFailed) {
    console.log("Test tcp media network");
    initiateMediaConnexion("tcp",
        function(connected) {
            if (connected) {
                message += "TCP : OK<br>";
                if (oneFailed)
                    setError(index, message);
                else
                    setSuccess(index, message);
            } else {
                message += "TCP : KO<br>";
                setError(index, message);
            }
            testRunning = false;
        },
        function(error) {
            message += "TCP : KO<br>";
            setError(index, message);
            testRunning = false;
        }
    );
}