/* js/components/webrtc_utils.js */
/**
 * global peerconnection objects
 */
var pc1;
var pc2;

/**
 * global local media stream object
 */
var localStreamGlobal;

/**
 * global peerconnection stats objects
 */
var getStatsTime = 500;
var stateEnabled = false;
var timestampPrev = 0;
var bytesPrev = 0;
var packetLostPrev = 0;

var byteRate = 0;
var packetLost = 0;
var fractionLost = 0;
var jitter = 0;


/**
 * Turn media relay informations
 */
var tcpTurnServer = { 'username': '', 'credential': '', 'urls': 'turn:joona.icu?transport=tcp' }; // fastest found TURN server for tcp
var udpTurnServer = { 'username': '', 'credential': '', 'urls': 'turn:joona.icu?transport=udp' }; // fastest found TURN server for tcp


/**
 * init turnServer credtials with short term credential fill by Server request to turn.geant.org
 */
function initTurnServerCerdentials() {


    tcpTurnServer.username = iceServer[0].username;
    tcpTurnServer.credential = iceServer[0].credential;
    tcpTurnServer.urls = iceServer[0].tcpTestUrl;


    udpTurnServer.username = iceServer[0].username;
    udpTurnServer.credential = iceServer[0].credential;
    udpTurnServer.urls = iceServer[0].udpTestUrl;

}

/**
 * Test if mediadevice API is present
 * @returns {boolean}
 */
function isGetUserMediaSupported() {
    if (navigator.mediaDevices === undefined) {
        return false;
    }
    return true;
}

/**
 * Test if RTCPeerConnection API is present
 * @returns {boolean}
 */
function isPeerConnectionSupported() {
    var rtcConfig = {};
    try {
        var pc = new RTCPeerConnection(rtcConfig);
        if (pc === undefined) {
            return false;
        }
        return true;
    } catch (err) {
        return false;
    }
}

/**
 * Test if DataChannel API is present
 * @returns {boolean}
 */
function isDataChannelSupported() {
    var rtcConfig = {};
    const dcInitOptions = {
        ordered: true,
        maxRetransmits: 65535,
        priority: "low",
        binaryType: "blob",
    };
    try {
        var pc = new RTCPeerConnection(rtcConfig);
        var dc = pc.createDataChannel("chat", dcInitOptions);
        if (dc === undefined) {
            return false;
        }
        return true;
    } catch (err) {
        return false;
    }
}

/**
 * Test if Browser support is part of banned browser
 * @returns {boolean}
 */
function isBannedBrowser() {
    if (bowser.msie || bowser.safari || bowser.msedge)
        return true;
    return false;
}

/**
 * Get All Media devices seen by the navigator
 * @param {callback} successCallback - callback that handles a mediaInfo object containing media labels
 * @param {callback} errorCallback - callback that handles the error message
 */
function getListDevices(successCallback, errorCallback) {
    navigator.mediaDevices.enumerateDevices()
        .then(function(devices) {
            var mediaInfo = {
                audioinput: [],
                audiooutput: [],
                videoinput: []
            };
            console.log(devices);
            devices.forEach(function(deviceInfo) {
                if (deviceInfo.kind === 'audioinput')
                    mediaInfo.audioinput.push(deviceInfo.label);
                else if (deviceInfo.kind === 'audiooutput')
                    mediaInfo.audiooutput.push(deviceInfo.label);
                else if (deviceInfo.kind === 'videoinput')
                    mediaInfo.videoinput.push(deviceInfo.label);
            });
            successCallback(mediaInfo);
        })
        .catch(function(err) {
            errorCallback(err);
        });
}

/**
 * try to capture a media by its type(audio or video)
 * @param {string} mediaType - selected mediatype : audio|video
 * @param {callback} successCallback - callback that handles capture track labels
 * @param {callback} errorCallback - callback that handles the error message
 */
function getDefaultMediaCapture(mediaType, successCallback, errorCallback) {
    const mediaStreamConstraints = {
        video: false,
        audio: false
    };
    if (mediaType === "video") {
        mediaStreamConstraints.video = true;
    } else if (mediaType === "audio") {
        mediaStreamConstraints.audio = true;
    }
    navigator.mediaDevices.getUserMedia(mediaStreamConstraints)
        .then(function(mediaStream) {
            /* use the stream */
            var localtracks = mediaStream.getTracks();
            var tracklabels = "";
            localtracks.forEach(function(track) {
                tracklabels += track.label;
                track.stop();
            });
            successCallback(tracklabels);
        })
        .catch(function(err) {
            errorCallback(err);
        });
}

/**
 * try to initiate an Audio and Video webrtc peerconnection using different protocls
 * @param {string} protocol - selected transport realy protocol : udp|tcp
 * @param {callback} successCallback - callback that handles capture track labels
 * @param {callback} errorCallback - callback that handles the error message
 */
function initiateMediaConnexion(protocol, successCallback, errorCallback) {
    var mediaStreamConstraints = {
        video: false,
        audio: true
    };
    var rtcConfig = {
        iceTransportPolicy: 'relay'
    };
    if (protocol === "tcp")
        rtcConfig.iceServers = [tcpTurnServer];
    else
        rtcConfig.iceServers = [udpTurnServer];
    navigator.mediaDevices.getUserMedia(mediaStreamConstraints)
        .then(function(mediaStream) {
            /* use the stream */
            localStreamGlobal = mediaStream;

            var localVideo = $("#localvideo")[0];
            localVideo.srcObject = mediaStream;
            localVideo.onloadedmetadata = function(e) {
                localVideo.play();
            };
            call(rtcConfig)
                .then(function() {
                    wait(2000).then(function() { return hangup(successCallback, errorCallback); });
                });
        })
        .catch(function(err) {
            errorCallback(err);
        })
}


/**
 * try to initiate an Audio and Video webrtc peerconnection using different protocls
 * @param {rtcConfig} rtcConfig - Object containing inforamtion about peerconneciton configuration
 */
function call(rtcConfig) {
    console.log('Call Start');
    var remoteVideo = $("#remotevideo")[0];
    var videoTracks = localStreamGlobal.getVideoTracks();
    var audioTracks = localStreamGlobal.getAudioTracks();
    if (audioTracks.length > 0) {
        console.log('Actual Audio device: ' + audioTracks[0].label);
    }

    if (videoTracks.length > 0) {
        console.log('Actual Video device: ' + videoTracks[0].label);
    }
    pc1 = new RTCPeerConnection(rtcConfig);
    pc2 = new RTCPeerConnection(rtcConfig);
    console.log('create peer connection objects');

    // signaling state
    var signalingStateLog1 = pc1.signalingState;
    var signalingStateLog2 = pc2.signalingState;

    pc1.onsignalingstatechange = function() {
        if (pc1) {
            signalingStateLog1 += " -> " + pc1.signalingState;
            console.log('PC1 Sinaling: ' + signalingStateLog1);
        }
    };

    pc2.onsignalingstatechange = function() {
        if (pc2) {
            signalingStateLog2 += " -> " + pc2.signalingState;
            console.log('PC2 Sinaling: ' + signalingStateLog2);
        }
    };

    // ice state
    var iceConnectionStateLog1 = pc1.iceConnectionState;
    var iceConnectionStateLog2 = pc2.iceConnectionState;

    pc1.oniceconnectionstatechange = function() {
        if (pc1) {
            iceConnectionStateLog1 += " -> " + pc1.iceConnectionState
            console.log('PC1 ICE: ' + iceConnectionStateLog1);
        }
    };

    pc2.oniceconnectionstatechange = function() {
        if (pc2) {
            iceConnectionStateLog2 += " -> " + pc2.iceConnectionState
            console.log('PC2 ICE: ' + iceConnectionStateLog2);
        }
    };

    pc1.onicecandidate = function(event) {
        pc2.addIceCandidate(event.candidate);
        if (event.candidate) {
            console.log('pc1 ICE Candidate: ' + event.candidate.candidate);
        }
    }

    pc2.onicecandidate = function(event) {
        pc1.addIceCandidate(event.candidate);
        if (event.candidate) {
            console.log('pc2 ICE Candidate: ' + event.candidate.candidate);
        }
    }

    pc2.ontrack = function(event) {
        remoteVideo.srcObject = event.streams[0];
        remoteVideo.onloadedmetadata = function(e) {
            remoteVideo.play();
        };
    }
    pc2.onnegotiationneeded = function(event) {
        try {
            console.log("pc2");
        } catch (err) {
            console.error(err);
        }
    };

    // add mediastream
    localStreamGlobal.getTracks().forEach(function(track) { pc1.addTrack(track, localStreamGlobal); });

    var offerOptions = {
        offerToReceiveAudio: 1,
        offerToReceiveVideo: 1
    };

    // init call
    return pc1.createOffer(offerOptions).then(function(offer) {
        pc1.setLocalDescription(offer);
        pc2.setRemoteDescription(offer);
        console.log('Offer: ' + offer.sdp);
        pc2.createAnswer().then(function(answer) {
            pc2.setLocalDescription(answer);
            pc1.setRemoteDescription(answer);
            console.log('Answer: ' + answer.sdp);
            stateEnabled = true;
            timestampPrev = 0;
            bytesPrev = 0;
            packetLostPrev = 0;
            getterStats(pc2);
        });
    });
}

/**
 * Promise stype timeout fonction
 */
function wait(ms) {
    return new Promise(function(resolve) { return setTimeout(resolve, ms) });
}

/**
 * Start the peerconnection getuser process
 * @param {Peerconneciton} Peerconnection
 */
function getterStats(peerconnection) {
    if (stateEnabled) {
        wait(getStatsTime).then(function() {
                peerconnection.getStats().then(function(report) {
                        if (report)
                            writeStats(report);
                        getterStats(peerconnection);
                    })
                    .catch(function(err) {
                        console.log(err);
                    })
            })
            .catch(function(err) {
                console.log(err);
            });
    }
}

/**
 * Write some peerconnection statistics like packet loss or received bandwidth into global variable
 * @param {PeerconnecitonStatsreport} result - Peerconnction statistique report for a specific timeStamp
 */
function writeStats(results) {
    results.forEach(function(report) {
        var now = report.timestamp;
        if (report.type === 'inbound-rtp' && report.mediaType === 'video') {
            var bytes = report.bytesReceived;
            if (timestampPrev) {
                byteRate = 8 * (bytes - bytesPrev) / (now - timestampPrev);
                byteRate = Math.floor(byteRate);
            }
            bytesPrev = bytes;
            timestampPrev = now;
        }
    });
    results.forEach(function(report) {
        if (report.type === 'inbound-rtp') {
            packetLost = report.packetsLost;
            fractionLost = report.fractionLost;
            jitter = report.jitter;
        }
    });
}


/**
 * Close the 2 active peerconneciton pc1 and pc2 and release media capture
 * @param {callback} successCallback - callback that handles end call status
 * @param {callback} errorCallback - callback that handles the error message
 */
function hangup(successCallback, errorCallback) {
    console.log("pc1 iceConnectionState :" + pc1.iceConnectionState);
    console.log("pc2 iceConnectionState :" + pc2.iceConnectionState);
    stateEnabled = false;
    var connected = (pc2.iceConnectionState === "connected");
    pc1.close();
    pc2.close();
    localStreamGlobal.getTracks().forEach(function(track) { track.stop(); });
    successCallback(connected);
}