function checkRoomName(roomName, checkEmpty) {
    //var regex = new RegExp(/([^A-Za-z0-9\_])/);
    //var regex = new RegExp(/([^A-Za-z0-9\_#\.=\&])/);
    var regex = new RegExp(/([\/\\])/);

    if (roomName !== "") {
        if (regex.test(roomName)) {
            $('#roomNameError').addClass('show-error');
            $('#RoomName').addClass('show-error');
            $('#roomNameError text').text('Les caractères "\/" et "\\" sont interdits.');
            //alert('Nom de salon incorrect.\nLe nom ne doit pas comporter de caractères spéciaux.');
            return false;
        }
    } else if (checkEmpty == true) {
        $('#roomNameError').addClass('show-error');
        $('#RoomName').addClass('show-error');
        $('#roomNameError text').text('Un nom de salon est obligatoire.');
        //alert("Vous n'avez entré aucun nom de salon");
        return false;
    }

    $('#roomNameError').removeClass('show-error');
    $('#roomNameError').addClass('hide-error');
    $('#RoomName').removeClass('show-error');
    $('#roomNameError text').text('');
    return true;
}

/**
 * Go to the room with the given name
 * 
 */
function goToRoom(roomName) {
    if (checkRoomName(roomName, true) == true) {

        console.log(roomName);

        window.localStorage.clear();
        document.location.href = "/" + roomName + document.location.search;

        return true;
    }

    return roomName;
}

/**
 * Generate a random name and send it to room
 * 
 */
function generateRandomName(gotoRoom = true) {
    var roomName = generateRoomWithoutSeparator();
    //checkRoomName(roomName);

    if (gotoRoom)
        return goToRoom(roomName);
    else
        return roomName;
}