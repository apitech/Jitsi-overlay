/*!
 * EventEmitter v5.2.8 - git.io/ee
 * Unlicense - http://unlicense.org/
 * Oliver Caldwell - https://oli.me.uk/
 * @preserve
 */

;
(function(exports) {
    'use strict';

    /**
     * Class for managing events.
     * Can be extended to provide event functionality in other classes.
     *
     * @class EventEmitter Manages event registering and emitting.
     */
    function EventEmitter() {}

    // Shortcuts to improve speed and size
    var proto = EventEmitter.prototype;
    var originalGlobalValue = exports.EventEmitter;

    /**
     * Finds the index of the listener for the event in its storage array.
     *
     * @param {Function[]} listeners Array of listeners to search through.
     * @param {Function} listener Method to look for.
     * @return {Number} Index of the specified listener, -1 if not found
     * @api private
     */
    function indexOfListener(listeners, listener) {
        var i = listeners.length;
        while (i--) {
            if (listeners[i].listener === listener) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Alias a method while keeping the context correct, to allow for overwriting of target method.
     *
     * @param {String} name The name of the target method.
     * @return {Function} The aliased method
     * @api private
     */
    function alias(name) {
        return function aliasClosure() {
            return this[name].apply(this, arguments);
        };
    }

    /**
     * Returns the listener array for the specified event.
     * Will initialise the event object and listener arrays if required.
     * Will return an object if you use a regex search. The object contains keys for each matched event. So /ba[rz]/ might return an object containing bar and baz. But only if you have either defined them with defineEvent or added some listeners to them.
     * Each property in the object response is an array of listener functions.
     *
     * @param {String|RegExp} evt Name of the event to return the listeners from.
     * @return {Function[]|Object} All listener functions for the event.
     */
    proto.getListeners = function getListeners(evt) {
        var events = this._getEvents();
        var response;
        var key;

        // Return a concatenated array of all matching events if
        // the selector is a regular expression.
        if (evt instanceof RegExp) {
            response = {};
            for (key in events) {
                if (events.hasOwnProperty(key) && evt.test(key)) {
                    response[key] = events[key];
                }
            }
        } else {
            response = events[evt] || (events[evt] = []);
        }

        return response;
    };

    /**
     * Takes a list of listener objects and flattens it into a list of listener functions.
     *
     * @param {Object[]} listeners Raw listener objects.
     * @return {Function[]} Just the listener functions.
     */
    proto.flattenListeners = function flattenListeners(listeners) {
        var flatListeners = [];
        var i;

        for (i = 0; i < listeners.length; i += 1) {
            flatListeners.push(listeners[i].listener);
        }

        return flatListeners;
    };

    /**
     * Fetches the requested listeners via getListeners but will always return the results inside an object. This is mainly for internal use but others may find it useful.
     *
     * @param {String|RegExp} evt Name of the event to return the listeners from.
     * @return {Object} All listener functions for an event in an object.
     */
    proto.getListenersAsObject = function getListenersAsObject(evt) {
        var listeners = this.getListeners(evt);
        var response;

        if (listeners instanceof Array) {
            response = {};
            response[evt] = listeners;
        }

        return response || listeners;
    };

    function isValidListener(listener) {
        if (typeof listener === 'function' || listener instanceof RegExp) {
            return true
        } else if (listener && typeof listener === 'object') {
            return isValidListener(listener.listener)
        } else {
            return false
        }
    }

    /**
     * Adds a listener function to the specified event.
     * The listener will not be added if it is a duplicate.
     * If the listener returns true then it will be removed after it is called.
     * If you pass a regular expression as the event name then the listener will be added to all events that match it.
     *
     * @param {String|RegExp} evt Name of the event to attach the listener to.
     * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.addListener = function addListener(evt, listener) {
        if (!isValidListener(listener)) {
            throw new TypeError('listener must be a function');
        }

        var listeners = this.getListenersAsObject(evt);
        var listenerIsWrapped = typeof listener === 'object';
        var key;

        for (key in listeners) {
            if (listeners.hasOwnProperty(key) && indexOfListener(listeners[key], listener) === -1) {
                listeners[key].push(listenerIsWrapped ? listener : {
                    listener: listener,
                    once: false
                });
            }
        }

        return this;
    };

    /**
     * Alias of addListener
     */
    proto.on = alias('addListener');

    /**
     * Semi-alias of addListener. It will add a listener that will be
     * automatically removed after its first execution.
     *
     * @param {String|RegExp} evt Name of the event to attach the listener to.
     * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.addOnceListener = function addOnceListener(evt, listener) {
        return this.addListener(evt, {
            listener: listener,
            once: true
        });
    };

    /**
     * Alias of addOnceListener.
     */
    proto.once = alias('addOnceListener');

    /**
     * Defines an event name. This is required if you want to use a regex to add a listener to multiple events at once. If you don't do this then how do you expect it to know what event to add to? Should it just add to every possible match for a regex? No. That is scary and bad.
     * You need to tell it what event names should be matched by a regex.
     *
     * @param {String} evt Name of the event to create.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.defineEvent = function defineEvent(evt) {
        this.getListeners(evt);
        return this;
    };

    /**
     * Uses defineEvent to define multiple events.
     *
     * @param {String[]} evts An array of event names to define.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.defineEvents = function defineEvents(evts) {
        for (var i = 0; i < evts.length; i += 1) {
            this.defineEvent(evts[i]);
        }
        return this;
    };

    /**
     * Removes a listener function from the specified event.
     * When passed a regular expression as the event name, it will remove the listener from all events that match it.
     *
     * @param {String|RegExp} evt Name of the event to remove the listener from.
     * @param {Function} listener Method to remove from the event.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.removeListener = function removeListener(evt, listener) {
        var listeners = this.getListenersAsObject(evt);
        var index;
        var key;

        for (key in listeners) {
            if (listeners.hasOwnProperty(key)) {
                index = indexOfListener(listeners[key], listener);

                if (index !== -1) {
                    listeners[key].splice(index, 1);
                }
            }
        }

        return this;
    };

    /**
     * Alias of removeListener
     */
    proto.off = alias('removeListener');

    /**
     * Adds listeners in bulk using the manipulateListeners method.
     * If you pass an object as the first argument you can add to multiple events at once. The object should contain key value pairs of events and listeners or listener arrays. You can also pass it an event name and an array of listeners to be added.
     * You can also pass it a regular expression to add the array of listeners to all events that match it.
     * Yeah, this function does quite a bit. That's probably a bad thing.
     *
     * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add to multiple events at once.
     * @param {Function[]} [listeners] An optional array of listener functions to add.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.addListeners = function addListeners(evt, listeners) {
        // Pass through to manipulateListeners
        return this.manipulateListeners(false, evt, listeners);
    };

    /**
     * Removes listeners in bulk using the manipulateListeners method.
     * If you pass an object as the first argument you can remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
     * You can also pass it an event name and an array of listeners to be removed.
     * You can also pass it a regular expression to remove the listeners from all events that match it.
     *
     * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to remove from multiple events at once.
     * @param {Function[]} [listeners] An optional array of listener functions to remove.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.removeListeners = function removeListeners(evt, listeners) {
        // Pass through to manipulateListeners
        return this.manipulateListeners(true, evt, listeners);
    };

    /**
     * Edits listeners in bulk. The addListeners and removeListeners methods both use this to do their job. You should really use those instead, this is a little lower level.
     * The first argument will determine if the listeners are removed (true) or added (false).
     * If you pass an object as the second argument you can add/remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
     * You can also pass it an event name and an array of listeners to be added/removed.
     * You can also pass it a regular expression to manipulate the listeners of all events that match it.
     *
     * @param {Boolean} remove True if you want to remove listeners, false if you want to add.
     * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add/remove from multiple events at once.
     * @param {Function[]} [listeners] An optional array of listener functions to add/remove.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.manipulateListeners = function manipulateListeners(remove, evt, listeners) {
        var i;
        var value;
        var single = remove ? this.removeListener : this.addListener;
        var multiple = remove ? this.removeListeners : this.addListeners;

        // If evt is an object then pass each of its properties to this method
        if (typeof evt === 'object' && !(evt instanceof RegExp)) {
            for (i in evt) {
                if (evt.hasOwnProperty(i) && (value = evt[i])) {
                    // Pass the single listener straight through to the singular method
                    if (typeof value === 'function') {
                        single.call(this, i, value);
                    } else {
                        // Otherwise pass back to the multiple function
                        multiple.call(this, i, value);
                    }
                }
            }
        } else {
            // So evt must be a string
            // And listeners must be an array of listeners
            // Loop over it and pass each one to the multiple method
            i = listeners.length;
            while (i--) {
                single.call(this, evt, listeners[i]);
            }
        }

        return this;
    };

    /**
     * Removes all listeners from a specified event.
     * If you do not specify an event then all listeners will be removed.
     * That means every event will be emptied.
     * You can also pass a regex to remove all events that match it.
     *
     * @param {String|RegExp} [evt] Optional name of the event to remove all listeners for. Will remove from every event if not passed.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.removeEvent = function removeEvent(evt) {
        var type = typeof evt;
        var events = this._getEvents();
        var key;

        // Remove different things depending on the state of evt
        if (type === 'string') {
            // Remove all listeners for the specified event
            delete events[evt];
        } else if (evt instanceof RegExp) {
            // Remove all events matching the regex.
            for (key in events) {
                if (events.hasOwnProperty(key) && evt.test(key)) {
                    delete events[key];
                }
            }
        } else {
            // Remove all listeners in all events
            delete this._events;
        }

        return this;
    };

    /**
     * Alias of removeEvent.
     *
     * Added to mirror the node API.
     */
    proto.removeAllListeners = alias('removeEvent');

    /**
     * Emits an event of your choice.
     * When emitted, every listener attached to that event will be executed.
     * If you pass the optional argument array then those arguments will be passed to every listener upon execution.
     * Because it uses `apply`, your array of arguments will be passed as if you wrote them out separately.
     * So they will not arrive within the array on the other side, they will be separate.
     * You can also pass a regular expression to emit to all events that match it.
     *
     * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
     * @param {Array} [args] Optional array of arguments to be passed to each listener.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.emitEvent = function emitEvent(evt, args) {
        var listenersMap = this.getListenersAsObject(evt);
        var listeners;
        var listener;
        var i;
        var key;
        var response;

        for (key in listenersMap) {
            if (listenersMap.hasOwnProperty(key)) {
                listeners = listenersMap[key].slice(0);

                for (i = 0; i < listeners.length; i++) {
                    // If the listener returns true then it shall be removed from the event
                    // The function is executed either with a basic call or an apply if there is an args array
                    listener = listeners[i];

                    if (listener.once === true) {
                        this.removeListener(evt, listener.listener);
                    }

                    response = listener.listener.apply(this, args || []);

                    if (response === this._getOnceReturnValue()) {
                        this.removeListener(evt, listener.listener);
                    }
                }
            }
        }

        return this;
    };

    /**
     * Alias of emitEvent
     */
    proto.trigger = alias('emitEvent');

    /**
     * Subtly different from emitEvent in that it will pass its arguments on to the listeners, as opposed to taking a single array of arguments to pass on.
     * As with emitEvent, you can pass a regex in place of the event name to emit to all events that match it.
     *
     * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
     * @param {...*} Optional additional arguments to be passed to each listener.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.emit = function emit(evt) {
        var args = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(evt, args);
    };

    /**
     * Sets the current value to check against when executing listeners. If a
     * listeners return value matches the one set here then it will be removed
     * after execution. This value defaults to true.
     *
     * @param {*} value The new value to check for when executing listeners.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.setOnceReturnValue = function setOnceReturnValue(value) {
        this._onceReturnValue = value;
        return this;
    };

    /**
     * Fetches the current value to check against when executing listeners. If
     * the listeners return value matches this one then it should be removed
     * automatically. It will return true by default.
     *
     * @return {*|Boolean} The current value to check for or the default, true.
     * @api private
     */
    proto._getOnceReturnValue = function _getOnceReturnValue() {
        if (this.hasOwnProperty('_onceReturnValue')) {
            return this._onceReturnValue;
        } else {
            return true;
        }
    };

    /**
     * Fetches the events object and creates one if required.
     *
     * @return {Object} The events storage object.
     * @api private
     */
    proto._getEvents = function _getEvents() {
        return this._events || (this._events = {});
    };

    /**
     * Reverts the global {@link EventEmitter} to its previous value and returns a reference to this version.
     *
     * @return {Function} Non conflicting EventEmitter class.
     */
    EventEmitter.noConflict = function noConflict() {
        exports.EventEmitter = originalGlobalValue;
        return EventEmitter;
    };

    // Expose the class either via AMD, CommonJS or the global object
    if (typeof define === 'function' && define.amd) {
        define(function() {
            return EventEmitter;
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = EventEmitter;
    } else {
        exports.EventEmitter = EventEmitter;
    }
}(typeof window !== 'undefined' ? window : this || {}));










let id = 0;

function parseSizeParam(value) {
    let parsedValue;

    // This regex parses values of the form 100px, 100em, 100pt or 100%.
    // Values like 100 or 100px are handled outside of the regex, and
    // invalid values will be ignored and the minimum will be used.
    const re = /([0-9]*\.?[0-9]+)(em|pt|px|%)$/;

    if (typeof value === 'string' && String(value).match(re) !== null) {
        parsedValue = value;
    } else if (typeof value === 'number') {
        parsedValue = `${value}px`;
    }

    return parsedValue;
}

const URI_PROTOCOL_PATTERN = '^([a-z][a-z0-9\\.\\+-]*:)';
const _URI_AUTHORITY_PATTERN = '(//[^/?#]+)';
const _URI_PATH_PATTERN = '([^?#]*)';

function generateURL(domain, options = {}) {
    return urlObjectToString({
        ...options,
        url: `https://${domain}/#jitsi_meet_external_api_id=${id}`
    });
}

function _standardURIToString(thiz) {
    // eslint-disable-next-line no-invalid-this
    const { hash, host, pathname, protocol, search } = thiz || this;
    let str = '';

    protocol && (str += protocol);

    // TODO userinfo

    host && (str += `//${host}`);
    str += pathname || '/';
    search && (str += search);
    hash && (str += hash);

    return str;
}

function parseStandardURIString(str) {
    /* eslint-disable no-param-reassign */

    const obj = {
        toString: _standardURIToString
    };

    let regex;
    let match;

    // XXX A URI string as defined by RFC 3986 does not contain any whitespace.
    // Usually, a browser will have already encoded any whitespace. In order to
    // avoid potential later problems related to whitespace in URI, strip any
    // whitespace. Anyway, the Jitsi Meet app is not known to utilize unencoded
    // whitespace so the stripping is deemed safe.
    str = str.replace(/\s/g, '');

    // protocol
    regex = new RegExp(URI_PROTOCOL_PATTERN, 'gi');
    match = regex.exec(str);
    if (match) {
        obj.protocol = match[1].toLowerCase();
        str = str.substring(regex.lastIndex);
    }
    // authority
    regex = new RegExp(`^${_URI_AUTHORITY_PATTERN}`, 'gi');
    match = regex.exec(str);
    if (match) {
        let authority = match[1].substring( /* // */ 2);

        str = str.substring(regex.lastIndex);

        // userinfo
        const userinfoEndIndex = authority.indexOf('@');

        if (userinfoEndIndex !== -1) {
            authority = authority.substring(userinfoEndIndex + 1);
        }

        obj.host = authority;

        // port
        const portBeginIndex = authority.lastIndexOf(':');

        if (portBeginIndex !== -1) {
            obj.port = authority.substring(portBeginIndex + 1);
            authority = authority.substring(0, portBeginIndex);
        }
        // hostname
        obj.hostname = authority;
    }
    // pathname
    regex = new RegExp(`^${_URI_PATH_PATTERN}`, 'gi');
    match = regex.exec(str);

    let pathname;

    if (match) {
        pathname = match[1];
        str = str.substring(regex.lastIndex);
    }
    if (pathname) {
        pathname.startsWith('/') || (pathname = `/${pathname}`);
    } else {
        pathname = '/';
    }
    obj.pathname = pathname;

    // query
    if (str.startsWith('?')) {
        let hashBeginIndex = str.indexOf('#', 1);

        if (hashBeginIndex === -1) {
            hashBeginIndex = str.length;
        }
        obj.search = str.substring(0, hashBeginIndex);
        str = str.substring(hashBeginIndex);
    } else {
        obj.search = ''; // Google Chrome
    }
    // fragment
    obj.hash = str.startsWith('#') ? str : '';

    /* eslint-enable no-param-reassign */

    return obj;
}

function _fixURIStringScheme(uri) {
    const regex = new RegExp(`${URI_PROTOCOL_PATTERN}+`, 'gi');
    const match = regex.exec(uri);

    if (match) {
        // As an implementation convenience, pick up the last scheme and make
        // sure that it is a well-known one.
        let protocol = match[match.length - 1].toLowerCase();

        if (protocol !== 'http:' && protocol !== 'https:') {
            protocol = 'https:';
        }

        /* eslint-disable no-param-reassign */

        uri = uri.substring(regex.lastIndex);
        if (uri.startsWith('//')) {
            // The specified URL was not a room name only, it contained an
            // authority.
            uri = protocol + uri;
        }
        /* eslint-enable no-param-reassign */
    }

    return uri;
}

function _objectToURLParamsArray(obj = {}) {
    const params = [];

    for (const key in obj) { // eslint-disable-line guard-for-in
        try {
            params.push(
                `${key}=${encodeURIComponent(JSON.stringify(obj[key]))}`);
        } catch (e) {
            console.warn(`Error encoding ${key}: ${e}`);
        }
    }

    return params;
}

function urlObjectToString(o) {
    // First normalize the given url. It come as o.url or split into o.serverURL
    // and o.room.
    let tmp;

    if (o.serverURL && o.room) {
        tmp = new URL(o.room, o.serverURL).toString();
    } else if (o.room) {
        tmp = o.room;
    } else {
        tmp = o.url || '';
    }

    const url = parseStandardURIString(_fixURIStringScheme(tmp));

    // protocol
    if (!url.protocol) {
        let protocol = o.protocol || o.scheme;

        if (protocol) {
            // Protocol is supposed to be the scheme and the final ':'. Anyway,
            // do not make a fuss if the final ':' is not there.
            protocol.endsWith(':') || (protocol += ':');
            url.protocol = protocol;
        }
    }

    // authority & pathname
    let { pathname } = url;

    if (!url.host) {
        // Web's ExternalAPI domain
        //
        // It may be host/hostname and pathname with the latter denoting the
        // tenant.
        const domain = o.domain || o.host || o.hostname;

        if (domain) {
            const { host, hostname, pathname: contextRoot, port } = parseStandardURIString(

                // XXX The value of domain in supposed to be host/hostname
                // and, optionally, pathname. Make sure it is not taken for
                // a pathname only.
                _fixURIStringScheme(`${APP_LINK_SCHEME}//${domain}`));

            // authority
            if (host) {
                url.host = host;
                url.hostname = hostname;
                url.port = port;
            }

            // pathname
            pathname === '/' && contextRoot !== '/' && (pathname = contextRoot);
        }
    }

    // pathname

    // Web's ExternalAPI roomName
    const room = o.roomName || o.room;

    if (room &&
        (url.pathname.endsWith('/') ||
            !url.pathname.endsWith(`/${room}`))) {
        pathname.endsWith('/') || (pathname += '/');
        pathname += room;
    }

    url.pathname = pathname;

    // query/search

    // Web's ExternalAPI jwt
    const { jwt } = o;

    if (jwt) {
        let { search } = url;

        if (search.indexOf('?jwt=') === -1 && search.indexOf('&jwt=') === -1) {
            search.startsWith('?') || (search = `?${search}`);
            search.length === 1 || (search += '&');
            search += `jwt=${jwt}`;

            url.search = search;
        }
    }

    // fragment/hash

    let { hash } = url;

    for (const urlPrefix of['config', 'interfaceConfig', 'devices', 'userInfo', 'appData']) {
        const urlParamsArray = _objectToURLParamsArray(
            o[`${urlPrefix}Overwrite`] ||
            o[urlPrefix] ||
            o[`${urlPrefix}Override`]);

        if (urlParamsArray.length) {
            let urlParamsString = `${urlPrefix}.${urlParamsArray.join(`&${urlPrefix}.`)}`;

            if (hash.length) {
                urlParamsString = `&${urlParamsString}`;
            } else {
                hash = '#';
            }
            hash += urlParamsString;
        }
    }

    url.hash = hash;

    return url.toString() || undefined;
}

class JitsiMeetExternalAPI extends EventEmitter {
    constructor(domain, options) {

        super();

        this._isApitechApi = true;

        this._domain = domain;
        this._url = generateURL(domain, options);

        if (options.parentNodeId) {
            this._parentNode = document.getElementById(options.parentNodeId);
        }
        else {
            this._parentNode = options.parentNode ? options.parentNode : document.body;
        }

        this._createIFrame(options.height, options.width, () => {

            // Send Message to NodeJs
            //  l'iframe ne se chargeait pas
            //this._sendMessage("init", { domain: domain, opts: options });
            var that = this;

            // Message received from nodejs
            window.onmessage = null;
            window.onmessage = (event) => {
                this._receiveMessage(event, that);
            };

            // Ne pas pas envoyer de type complexe par message
            if (options.parentNode){
                options.parentNodeId = options.parentNode.id;
                options.parentNode = null;
            }

            if (options.onload){
                this._onload = options.onload.bind({});
                options.onload = null;
            }

            //let configOverwrite = options.configOverwrite;
            //let interfaceConfigOverwrite = options.interfaceConfigOverwrite;

            setTimeout(function(){
                that._sendMessage("init", { domain: domain, opts: options });
                that._initialized=true;
            }, 1000);
        });
    }

    _onReceiveMessage(event) {
        this._receiveMessage(event, this);
    }

    _onload;

    // Message received from NodeJs
    _receiveMessage(event, context) {

        //if (event.origin !== window.origin)
        //    return;

        if (event.data.type == "videoConferenceJoined") {
            context.emit("videoConferenceJoined");
        } else if (event.data.type == "videoConferenceLeft") {
            context.emit("videoConferenceLeft");
        } else if (event.data.type == "ratingModal") {
            context.emit("ratingModal");
        } else if (event.data.type == "readyToClose") {
            context.emit("readyToClose");
        } else if (event.data.type == "initialized") {
            this._initialized = true;
        }
        else if (event.data.type == "onload") {
            if(this._onload){
                this._onload();
            }
        }
    }

    _initialized=false;

    delay(delayInms) {
        return new Promise(resolve => {
          setTimeout(() => {
            resolve(2);
          }, delayInms);
        });
      }

    async waitInitialization(){
        while (!this._initialized){
            await this.delay(100);
        }
    }

    // Send Message to NodeJs
    async _sendMessage(type, obj){
        if (type !== "init"){
            await this.waitInitialization();
        }

        obj.type = type;
        this._frame.contentWindow.postMessage(obj, '*');
        console.log("post message " + type);
    }

    _setSize(height, width) {

        const parsedHeight = parseSizeParam(height);
        const parsedWidth = parseSizeParam(width);

        if (parsedHeight !== undefined) {
            this._height = height;
            this._frame.style.height = parsedHeight;
        }

        if (parsedWidth !== undefined) {
            this._width = width;
            this._frame.style.width = parsedWidth;
        }
    }

    _createIFrame(height, width, onload) {
        const frameName = `jitsiConferenceFrame${id}`;

        this._frame = document.createElement('iframe');
        this._frame.allow = 'camera; microphone; display-capture; autoplay;';
        this._frame.src = this._url;
        this._frame.name = frameName;
        this._frame.id = frameName;
        this._setSize(height, width);
        this._frame.setAttribute('allowFullScreen', 'true');
        this._frame.style.border = 0;

        if (onload) {
            // waits for iframe resources to load
            // and fires event when it is done
            this._frame.onload = onload;
        }

        this._frame = this._parentNode.appendChild(this._frame);
    }

    dispose() {
        this._sendMessage("dispose", {});
    }

    executeCommands(commands) {
        this._sendMessage("executeCommands", {cmds : commands});
    }

    executeCommand(command, argument) {
        this._sendMessage("executeCommand", {cmd : command, arg : argument});
    }
}
