function writePlaceHolder(text, input) {

    var roomName = text.split('');
    //	var roomName = generateRoomWithoutSeparator().split('');

    (function animate() {
        roomName.length > 0 ? input.attr('placeholder', input.attr('placeholder') + roomName.shift()) : clearTimeout(running);
        var running = setTimeout(animate, 90);

    })();
};

/**
 * Animate the placeholder of 'input' with 'text', every 6500 ms *
 * @param  {string} text    The text to use in animation
 * @param  {object} input   The target object *
 * @return {int}            ID value returned by setInterval(), to use in clearInterval()
 */
function startAnimatedPlaceholder(text, input)
{
    writePlaceHolder(text, input);

    return setInterval(function()
    {
        input.attr('placeholder', '');
        writePlaceHolder(text, input);
    }, 6500);

};
