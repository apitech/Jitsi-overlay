let api;
let subject;

// Message received from apitech_layout_api.js
const receiveMessage = (event) => {

    //if (event.origin !== window.origin)
    //    return;

    if (event.data.type == "init") {
        onInit(event.data.domain, event.data.opts);
    } else if (api && event.data.type == "dispose") {
        api.dispose();
    } else if (api && event.data.type == "executeCommand") {
        if (event.data.cmd == "subject"){
          subject = event.data.arg;
        }

        api.executeCommand(event.data.cmd, event.data.arg);
    } else if (api && event.data.type == "executeCommands") {

        if (event.data.cmds.subject) {
          subject = event.data.cmds.subject;
          setTimeout(function() {
              api.executeCommand("subject", event.data.cmds.subject);
          }, 2000);
        }

        api.executeCommands(event.data.cmds);
    }
}

// Message send to apitech_layout_api.js
const sendMessage = (type, obj) => {
    obj.type = type;
    parent.postMessage(obj, '*');
}

const onVideoConferenceJoined = (event) => { sendMessage("videoConferenceJoined", {evt: event}); };

const onReadyToClose = () => {

  try { api._parentNode.ownerDocument.exitFullscreen(); }
  catch {}

  $("#modal_rate").on('hide.bs.modal', function() {

    var data = {
      clientID: 0,
      url: window.location.href,
      room: window.location.pathname.substring(1),
      evaluation: parseInt($('#evaluation').val()),
      comment: $('#comment').val(),
      date: null
    };

    if (data.comment != '' || data.evaluation > 0) {
      var json = JSON.stringify(data);

      $.ajax({
        type: 'post',
        url: '/Room/Rate',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: json
      });

      if (ratingPostUrl && ratingPostDataTemplate){

        let postData = ratingPostDataTemplate.replace('${date}', new Date())
                                              .replace('${room}', data.room)
                                              .replace('${comment}', data.comment.replace(/\n/g, '\\n'))
                                              .replace('${evaluation}', data.evaluation ? data.evaluation : '0');

        $.ajax({
          type: 'post',
          url: ratingPostUrl,
          crossDomain: true,
          contentType: "application/json; charset=utf-8",
          dataType: 'json',
          data: postData
        });

        console.warn('post ' + postData);
        console.warn('to ' + ratingPostUrl);
      }
    }

    sendMessage("videoConferenceLeft", {});
    sendMessage("evaluationSubmited", {
      evaluation: data
    });

    if (redirect_uri) {
      window.location.href = redirect_uri;
    }
  });

  sendMessage("ratingModal", {});
  sendMessage("readyToClose", {});

  $("#modal_rate").modal();
};

const onInit = (domain, options) => {

  options.parentNode = document.querySelector('#meet');

  if (!options.jwt && url_jwt) {
    options.jwt = url_jwt;
  }

    if (readonly_name === true) {
        if (!options.configOverwrite){
            options.configOverwrite = {};
        }

        options.configOverwrite.readOnlyName = readonly_name;
    }

    if (api) {
      api.removeListener("videoConferenceJoined", onVideoConferenceJoined);
      api.removeListener("readyToClose", onReadyToClose);

      api = null;
    }

    options.onload = () => { sendMessage("onload", {}); }
    api = new JitsiMeetExternalAPI(jitsi_domain + '?embedded=1', options);

    sendMessage("initialized", {});

  api.addListener("videoConferenceJoined", onVideoConferenceJoined);
  api.addListener("readyToClose", onReadyToClose);

  function hideAuthModal() { // little spinlock to hide the native jitsi auth modal when it comes up (keep it at 10 ms delay, above that the jitsi modal appear before our own)
    var iframeDoc = document.getElementById('meet').children[0].contentWindow.document;
    if (iframeDoc.getElementById("modal-dialog-ok-button") != null) {
      iframeDoc.getElementById("modal-dialog-ok-button").parentNode.parentNode.parentNode.parentNode.style["visibility"] = "hidden";
      iframeDoc.getElementById("modal-dialog-ok-button").parentNode.parentNode.parentNode.parentNode.style["content-visibility"] = "hidden";
    } else {
      setTimeout(modAuthButton, 10);
    }
  }


  function enableAuthModal() { // enable OUR auth modal if not already enabled, and hide the native jitsi one

    if (!$('#modal_wait_for_host').is(':visible')) {
      $("#modal_wait_for_host").modal({
        backdrop: 'static' // prevent closing while clicking outside it
      });
      hideAuthModal();
    }
  };

  function autoJoinLobby()
  {
    var iframeDoc = document.getElementById("meet").children[0].contentWindow.document;
    if(api._frame.contentWindow.APP !== undefined &&api._frame.contentWindow.APP.store !== undefined)
    {
      var st = api._frame.contentWindow.APP.store.getState()["features/base/conference"];
      if(iframeDoc.getElementsByClassName("content-controls").length >= 1 &&
         iframeDoc.getElementsByClassName("content-controls")[0] !== undefined &&
         iframeDoc.getElementsByClassName("content-controls")[0].children.length >= 2 &&
         iframeDoc.getElementsByClassName("content-controls")[0].children[2] !== undefined)
      {
        iframeDoc.getElementsByClassName("content-controls")[0].children[2].click();
      }
    }
    setTimeout(autoJoinLobby,50);
  };

  function disableAuthModal() { // disable OUR auth modal if not already disabled, we don't bother to reshow the native jitsi one, we never wanna see it anyway

    if ($('#modal_wait_for_host').is(':visible')) {
      $("#modal_wait_for_host").modal("hide");
      document.getElementById("jitsiConferenceFrame0").src = document.getElementById("jitsiConferenceFrame0").src+"&config.prejoinPageEnabled=false";
      setTimeout(autoJoinLobby,150);
    }
  };

  function toggleAuthModal() { // toggle visibility of OUR modal, we base ourselves on the Redux store to know when an authentication is required
    var st = api._frame.contentWindow.APP.store.getState()["features/base/conference"];
    if (st !== undefined && st.authRequired !== undefined) {
      enableAuthModal();
    } else {
      disableAuthModal();
    }
  };

  function subToRedux() { // launch a little spinlock to wait for the Redux store to be defined
    if (api._frame.contentWindow.APP !== undefined && api._frame.contentWindow.APP.store !== undefined) {
      api._frame.contentWindow.APP.store.subscribe(toggleAuthModal);
    } else {
      setTimeout(subToRedux, 100);
    }
  }

  if (oid_use) {
    subToRedux(); // launch the spinlock
  }


  function EditCSS() {
     if(api._frame !== undefined && api._frame.contentDocument !== undefined)
     {
        if (display_subject_on_prejoin && subject){
          var prejoinTitle = $(api._frame).contents().find("div.premeeting-screen .title");
          if (prejoinTitle){
            prejoinTitle.html(subject);
          }
        }

        if (hide_lobby_password_button){
          var lobbyEnterPasswordButton = $(api._frame).contents().find("div[data-testid='lobby.enterPasswordButton']");
          if (lobbyEnterPasswordButton){
            lobbyEnterPasswordButton.css("display","none");
          }
        }

        if (hide_displayName_field_ifjwt){
          var displayNameField = $(api._frame).contents().find("div[data-testid='prejoin.screen'] > input");

          if (displayNameField){
            if (displayNameField.val() === ''){
              api.executeCommand('displayName','Participant');
            }

            displayNameField.css("display","none");
          }
        }
    }
    setTimeout(EditCSS,100);
  }


    EditCSS();


}

$(document).ready(function() {
    // Message received from apitech_layout_api.js
    window.removeEventListener("message", receiveMessage, false);
    window.addEventListener("message", receiveMessage, false);

    if (window.location !== window.parent.location) {
        // The page is in an iframe
    } else {
        var room = window.location.pathname.substring(1);
        const domain = jitsi_domain + '?embedded=1';
        const options = {
            //roomName: '<%= room %>',
            roomName: room,
            //jwt: '<%= jwt %>',
            // interfaceConfigOverwrite: {
            //     filmStripOnly: true
            // },
            //configOverwrite: {
            //enableClosePage: true
            //defaultLanguage: 'en'
            //},
            parentNode: document.querySelector('#meet')
        };


        // The page is not in an iframe
        onInit(domain, options);
    }
});
